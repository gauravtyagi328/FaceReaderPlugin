package cordova.plugin.Facereader;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.content.Intent;
import ai.binah.facescan.MainActivity;
/**
 * This class echoes a string called from JavaScript.
 */
public class FaceReader extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("openCamera")) {
            String message = args.getString(0);
            this.openCamera(message, callbackContext);
            return true;
        }
        return false;
    }

    private void openCamera(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
        	Intent intentScan = new Intent(cordova.getActivity().getBaseContext(), MainActivity.class);
        	this.cordova.getActivity().startActivity(intentScan);
          callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
}
