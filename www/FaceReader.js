var exec = require('cordova/exec');

exports.openCamera = function (success, error) {
    exec(success, error, 'FaceReader', 'openCamera', ["openCamera"]);
};
